package com.app.ceresco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * Clase TypeIdentification.java Que representa una tabla en la base de datos con 
 * sus respectivos atributos
 * 
 * @author Vianey Vargas Morales
 *
 */

@Data
@Table(name = "tipo_identificacion")
@Entity
public class TypeIdentification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private String id;
	
	@NotEmpty(message = "El tipo de identificación no puede estar vacío")
	@Column(name = "tipo_identificacion", nullable = false, length = 5, unique = true)
	private String typeIdentification;
	
	@NotEmpty(message = "El nombre no puede estar vacío")
	@Column(name = "nombre", nullable = false, length = 25)
	private String name;

}
