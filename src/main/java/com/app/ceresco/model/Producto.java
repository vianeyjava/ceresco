package com.app.ceresco.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "productos")
public class Producto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private String id;

	@NotNull(message = "La Referencia del producto no puede ser vacio!")
	@Column(name = "referencia", unique = true, nullable = false, length = 30)
	private String referenceProduct;

	@NotNull(message = "La descripcion del producto no puede ser vacio!")
	@Size(min = 1, max = 100, message = "La descripcion debe tener entre {min} á {max} caracteres")
	@Column(name = "Descripcion", columnDefinition = "varchar(100)")
	private String description;
	
	@Column(name = "cantidad")
	private int cant;


}
