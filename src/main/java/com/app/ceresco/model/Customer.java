package com.app.ceresco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * Clase que cliente o POJO que representa una tabla en la base de datos con sus
 * respectivos atributos
 * 
 * @author vianey Vargas Morales
 * @date 07/01/2022
 *
 */
@Data
@Entity
@Table(name = "clientes")
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Es de tipo String para poderlo manejar prefesionalmente y a futuro como id universal
	 * en caso que la empresa solicite una migracion a otro motor de base de datos.
	 *
	 */
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "codigo")
	private String code;
	
	@Column(name = "nit")
	private String identification;
	
	@Column(name = "razon_social")
	private String businessName;
	
	@Column(name = "contacto")
	private String telPhone;
	
	@Column(name = "email")
	@NotEmpty(message = "El email no puede estar vacío")
	@Email(message = "Formato de correo electrónico no válido")
	private String email;
	
	@Column(name = "direccion")
	private String address;
	
	@Column(name = "telefono")
	private String tel;
	
	@ManyToOne
	@JoinColumn(name = "id_tipo_identificacion", nullable = true, referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_customer_tipo_identificacion_id"))
	private TypeIdentification typeOfIdentification;
	
	
	
	
	
	
	
	
	
	
	
	

}
