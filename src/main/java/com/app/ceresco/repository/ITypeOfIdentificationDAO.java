package com.app.ceresco.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.ceresco.model.TypeIdentification;

/**
 * ITypeOfIdentificationDAO.java interfaz que implementa la persistencia de
 * datos
 * 
 * @author vianeyvargasmorales
 * @version 1.0
 *
 */
@Repository
public interface ITypeOfIdentificationDAO extends JpaRepository<TypeIdentification, String> {

	
}
