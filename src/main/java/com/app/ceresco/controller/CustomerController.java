package com.app.ceresco.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.ceresco.model.Customer;
import com.app.ceresco.services.ICustomerService;


/**
 * Clase CustomerController.java Controller que permite hacer las peticiones
 * POST, DELETE, GET, UPDATE 
 * 
 * @author Vianey Vargas Morales
 * 
 *
 */
@RestController
@RequestMapping(value = {"/customer"})
public class CustomerController {
	
	@Autowired
	private ICustomerService service;
	
	
	
	@PostMapping(value = {"/save"}, produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<?> saveCustomer(@Valid @RequestBody Customer customer, BindingResult result){
		Customer object =null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {

			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			object= service.saveCustomer(customer);
		}catch(DataAccessException ex) {
			System.out.println("Error al registrar el cliente!"+ ex.getMessage());
			response.put("mensaje", "Ha ocurrido un error, inténtalo nuevamente.");
			response.put("error", ex.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El cliente ha sido registrado con éxito.");
		response.put("entityCustomer", object);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	/**
	 * Metodo para obtener los datos de todos los clientes registrados
	 * @author Vianey Vargas Morales
	 * @return
	 */
	@GetMapping(value = { "/findAll" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Customer>> findAll() {
		List<Customer> c = new ArrayList<>();
		c = service.findAll();
		return new ResponseEntity<List<Customer>>(c, HttpStatus.OK);
	}
	
	
	
}
