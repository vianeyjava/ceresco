package com.app.ceresco.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.ceresco.model.TypeIdentification;
import com.app.ceresco.services.ITypeIdentificationService;


@RestController
@RequestMapping(value = { "/tipoidentificacion" })
public class TypeOfIdentificationController {

	@Autowired(required = false)
    private ITypeIdentificationService service;

	@PostMapping(value = { "/save" }, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> register(@Valid @RequestBody TypeIdentification entity) {
		service.register(entity);
		URI url = ServletUriComponentsBuilder.fromCurrentRequest().path("{/id}").buildAndExpand(entity.getId()).toUri();
		return ResponseEntity.created(url).build();
	}

	@PutMapping(value = { "/update/{id}"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> update(@Valid @RequestBody TypeIdentification entity,
			@PathVariable("id") Integer id) {
		service.update(entity);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@GetMapping(value = {"/getAll" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TypeIdentification>> getAll() {
		List<TypeIdentification> list = new ArrayList<>();
		list = service.getAll();
		return new ResponseEntity<List<TypeIdentification>>(list, HttpStatus.OK);
	}


	@GetMapping(value = { "/count", "/c", "/contar" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public long count() {
		return service.count();
	}

}
