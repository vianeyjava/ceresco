package com.app.ceresco.services.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.app.ceresco.model.Customer;
import com.app.ceresco.repository.ICustomerRepository;
import com.app.ceresco.services.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService{
	
	@Autowired
	private ICustomerRepository repository;

	@Override
	public Page<Customer> listPageable(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}

	
	@Override
	public List<Customer> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		repository.deleteById(id);
	}

	@Override
	public Customer saveCustomer(Customer entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return repository.count();
	}

}
