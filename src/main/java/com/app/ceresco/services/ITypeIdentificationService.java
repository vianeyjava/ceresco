package com.app.ceresco.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.app.ceresco.model.TypeIdentification;


public interface ITypeIdentificationService {

	Page<TypeIdentification> listPageable(Pageable pageable);


	public TypeIdentification register(TypeIdentification entity);

	public void update(TypeIdentification entity);

	public void delete(String id);

	public TypeIdentification listById(String id);

	public List<TypeIdentification> getAll();

	public long count();
}
