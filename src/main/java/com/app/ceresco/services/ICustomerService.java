package com.app.ceresco.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.app.ceresco.model.Customer;


public interface ICustomerService {
	
	/**
	 * Metodo que permite paginar los registros
	 * @author Vianey Vargas Morales
	 * @param pageable
	 * @return
	 */
	public Page<Customer> listPageable(Pageable pageable);
	
	/**
	 * Metodo que permite obtener todos los registros actuales
	 * @author Vianey Vargas Morales
	 * @return
	 */
	public List<Customer> findAll();
	
	/**
	 * Metodo que permite eliminar un registro
	 * @author Vianey Vargas Morales
	 * @param id
	 */
	public void delete(String id);
	
	/**
	 * Metodo para registrar clientes
	 * @author Vianey Vargas Morales
	 * @param entity
	 * @return
	 */
	public Customer saveCustomer(Customer entity);
	
	/**
	 * Metodo que permite contar los registros
	 * @author Vianey Vargas Morales
	 * @return
	 */
	public long count();

}
